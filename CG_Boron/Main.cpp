#include "Common.h"

#include "Main.h"

#include "CPUID.h"
#include "resource.h"

namespace Boron
{
	const std::wstring Main::title = L"CG Assignment";
	const int32_t Main::windowWidth = 800;
	const int32_t Main::windowHeight = 600;

	Main::Main()
		: inst(nullptr), window(nullptr), iconSmall(nullptr), iconBig(nullptr)
		, gldc(nullptr), glrc(nullptr)
		, frameNumber(0)
		, active(false), currentViewportX(0), currentViewportY(0), currentViewportWidth(0), currentViewportHeight(0)
		, cameraEyeX(0.0), cameraEyeY(1.25), cameraEyeZ(1.5)
		, cameraAtX(0.0), cameraAtY(0.5), cameraAtZ(0.0)
		, cameraUpX(0.0), cameraUpY(1.0), cameraUpZ(0.0)
		, leftMouse(false), rightMouse(false), trackingMouse(false), clickOriginX(0), clickOriginY(0)
		, clickCameraEyeX(0.0), clickCameraEyeY(0.0)
		, clickCameraAtX(0.0), clickCameraAtY(0.0)
		, clickCameraXZRotation(0.0), clickCameraDistance(0.0)
		, prevTick(0), prevTime(0)
		, simul(Simulator::water)
		, anim(false)
	{
		memset(frames.data(), 0, frames.size() * sizeof(int32_t));
	}

	bool Main::run(HINSTANCE instance, const std::wstring &commandLine, int showCommand)
	{
#ifndef NO_SSE41
		if(!isSSE41Supported())
		{
			MessageBoxW(nullptr, L"Y U NO SSE 4.1? U CANNOT EXECUTE!", L"SSE 4.1 Support Required", MB_OK | MB_ICONERROR);
			return false;
		}
#endif

		inst = instance;

		WNDCLASSW wc = {0, };
		wc.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
		wc.lpfnWndProc = windowProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = instance;
		wc.hIcon = LoadIconW(nullptr, IDI_APPLICATION);
		wc.hCursor = LoadCursorW(nullptr, IDC_ARROW);
		wc.hbrBackground = nullptr;
		wc.lpszMenuName = nullptr; // MAKEINTRESOURCEW(IDR_MAIN);
		wc.lpszClassName = L"BoronClass";

		RegisterClassW(&wc);

		RECT rc = {0, 0, windowWidth, windowHeight};
		uint_fast32_t windowStyle = WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
		uint_fast32_t windowStyleEx = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		AdjustWindowRectEx(&rc, windowStyle, TRUE, windowStyleEx);

		window = CreateWindowExW(windowStyleEx, L"BoronClass", title.c_str(),
			windowStyle, CW_USEDEFAULT, CW_USEDEFAULT, windowWidth, windowHeight,
			nullptr, nullptr, instance, nullptr);

		ShowWindow(window, showCommand);
		UpdateWindow(window);

		// HACCEL accel = LoadAcceleratorsW(instance, MAKEINTRESOURCEW(IDR_MAIN));
		MSG msg;
		while(true)
		{
			if(PeekMessageW(&msg, nullptr, 0, 0, PM_REMOVE))
			{
				if(msg.message == WM_QUIT)
					break;
				// if(!TranslateAcceleratorW(window, accel, &msg))
				{
					TranslateMessage(&msg);
					DispatchMessageW(&msg);
				}
			}
			else
			{
				//if(active)
				{
					draw();
					SwapBuffers(gldc);
				}
			}
		}

		return true;
	}

	namespace
	{
		double sint1[20], cost1[20], sint2[20], cost2[20];

		void fghCircleTable(double *sint,double *cost,const int n)
		{
			int i;

			/* Table size, the sign of n flips the circle direction */

			const int size = abs(n);

			/* Determine the angle between samples */

			const double angle = 2*Utilities::Constants::PI/(double)( ( n == 0 ) ? 1 : n );

			/* Compute cos and sin around the circle */

			sint[0] = 0.0;
			cost[0] = 1.0;

			for (i=1; i<size; i++)
			{
				sint[i] = sin(angle*i);
				cost[i] = cos(angle*i);
			}

			/* Last sample is duplicate of the first */

			sint[size] = sint[0];
			cost[size] = cost[0];
		}

		/*
		 * Draws a solid sphere
		 */
		void glutSolidSphere(GLdouble radius, GLint slices, GLint stacks, double *sint1, double *cost1, double *sint2, double *cost2)
		{
			int i,j;

			/* Adjust z and radius as stacks are drawn. */

			double z0,z1;
			double r0,r1;

			/* Pre-computed circle */

			/* The top stack is covered with a triangle fan */

			z0 = 1.0;
			z1 = cost2[(stacks>0)?1:0];
			r0 = 0.0;
			r1 = sint2[(stacks>0)?1:0];

			glBegin(GL_TRIANGLE_FAN);

				glNormal3d(0,0,1);
				glVertex3d(0,0,radius);

				for (j=slices; j>=0; j--)
				{
					glNormal3d(cost1[j]*r1,        sint1[j]*r1,        z1       );
					glVertex3d(cost1[j]*r1*radius, sint1[j]*r1*radius, z1*radius);
				}

			glEnd();

			/* Cover each stack with a quad strip, except the top and bottom stacks */

			for( i=1; i<stacks-1; i++ )
			{
				z0 = z1; z1 = cost2[i+1];
				r0 = r1; r1 = sint2[i+1];

				glBegin(GL_QUAD_STRIP);

					for(j=0; j<=slices; j++)
					{
						glNormal3d(cost1[j]*r1,        sint1[j]*r1,        z1       );
						glVertex3d(cost1[j]*r1*radius, sint1[j]*r1*radius, z1*radius);
						glNormal3d(cost1[j]*r0,        sint1[j]*r0,        z0       );
						glVertex3d(cost1[j]*r0*radius, sint1[j]*r0*radius, z0*radius);
					}

				glEnd();
			}

			/* The bottom stack is covered with a triangle fan */

			z0 = z1;
			r0 = r1;

			glBegin(GL_TRIANGLE_FAN);

				glNormal3d(0,0,-1);
				glVertex3d(0,0,-radius);

				for (j=0; j<=slices; j++)
				{
					glNormal3d(cost1[j]*r0,        sint1[j]*r0,        z0       );
					glVertex3d(cost1[j]*r0*radius, sint1[j]*r0*radius, z0*radius);
				}

			glEnd();
		}
	}

	longptr_t Main::windowProcImpl(HWND window, uint32_t message, uintptr_t wParam, longptr_t lParam)
	{
		switch(message)
		{
		case WM_CREATE:
			{
				static const int32_t sbPartsSize[] =
				{
					80, // FPS
					160, // Shading Mode
					260, // Lighting 0 Mode
					360, // Lighting 1 Mode
					-1, // Other Status
				};

				this->window = window;

				iconSmall = static_cast<HICON>(LoadImageW(inst, MAKEINTRESOURCEW(IDI_MAIN), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR));
				iconBig = static_cast<HICON>(LoadImageW(inst, MAKEINTRESOURCEW(IDI_MAIN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR));

				SendMessageW(window, WM_SETICON, ICON_SMALL, reinterpret_cast<longptr_t>(iconSmall));
				SendMessageW(window, WM_SETICON, ICON_BIG, reinterpret_cast<longptr_t>(iconBig));

				simul.addCollision(std::shared_ptr<Collision::Object>(
					new Collision::OBB(false, Float3(0, 0.5f, 0), Float3(1.5f, 0, 0), Float3(0, 0.5f, 0), Float3(0, 0, 0.5f))
				));
				simul.addCollision(std::shared_ptr<Collision::Object>(
					new Collision::Capsule(true, Float3(0, 0, 0), Float3(0, 1.0f, 0), 0.15f)
				));
				simul.addCollision(std::shared_ptr<Collision::Object>(
					new Collision::Capsule(true, Float3(0.3f, 0, 0.3f), Float3(0.3f, 1.0f, 0.3f), 0.15f)
				));
				simul.addCollision(std::shared_ptr<Collision::Object>(
					new Collision::Capsule(true, Float3(0.3f, 0, -0.3f), Float3(0.3f, 1.0f, -0.3f), 0.15f)
				));
				simul.addCollision(std::shared_ptr<Collision::Object>(
					new Collision::Capsule(true, Float3(-0.3f, 0, 0.4f), Float3(-0.3f, 1.0f, 0.4f), 0.25f)
				));
				simul.addCollision(std::shared_ptr<Collision::Object>(
					new Collision::Capsule(true, Float3(-0.3f, 0, -0.4f), Float3(-0.3f, 1.0f, -0.4f), 0.25f)
				));

				static const float xr = 0.2f, yr = 0.5f, zr = 0.5f;
				static const float ptcDist = 0.0272f;

				Particle p = { Float3(0, 0, 0), Float3(0, 0, 0), Float3(0, 0, 0), Float3(0, 0, 0) };
				/*for(float x = -0.5f; x <= -0.5f + 2 * r; x += ptcDist)
				{
					for(float y = 0; y <= 2 * yr; y += ptcDist)
					{
						for(float z = -0.5f; z <= -0.5f + 2 * r; z += ptcDist)
						{
							p.pos = Float3(x, y, z);
							simul.addParticle(p);
						}
					}
				}*/

				for(float x = 1.5f; x >= 1.5f - 2 * xr; x -= ptcDist)
				{
					for(float y = 0; y <= 2 * yr; y += ptcDist)
					{
						for(float z = -zr; z <= zr; z += ptcDist)
						{
							p.pos = Float3(x, y, z);
							simul.addParticle(p);
						}
					}
				}

				initOpenGL();

				fghCircleTable(sint1,cost1,-5);
				fghCircleTable(sint2,cost2,5*2);

#ifdef CAPTURE_FRAMES
				wchar_t buf[100];
				SYSTEMTIME st;
				GetLocalTime(&st);
				swprintf_s(buf, L"%04d-%02d-%02d %02d-%02d-%02d", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
				capturePath = std::wstring(L"Captures/") + buf + L"/";
				createPath(capturePath);
#endif
			}
			return 0;

		case WM_ACTIVATE:
			if(HIWORD(wParam) != 0) // Minimized
				active = false;
			else
				active = true;
			return 0;

		case WM_GETMINMAXINFO:
			{
				MINMAXINFO *mmi = reinterpret_cast<MINMAXINFO *>(lParam);
				mmi->ptMinTrackSize.x = 630;
				mmi->ptMinTrackSize.y = 440;
			}
			return 0;

		case WM_SIZE:
			{
				resize(0, 0, LOWORD(lParam), HIWORD(lParam));

				draw();
			}
			return 0;

		case WM_KEYDOWN:
			if(wParam == VK_SPACE)
			{
				anim = !anim;
				return 0;
			}
			else if(wParam == VK_RETURN && !anim)
			{
				simul.nextFrame();
				return 0;
			}
			break;

#define POINTARG static_cast<int16_t>(LOWORD(lParam)), static_cast<int16_t>(HIWORD(lParam))
		case WM_LBUTTONDOWN:
			{
				clickMouse(true, true, POINTARG);
			}
			return 0;

		case WM_LBUTTONUP:
			{
				clickMouse(true, false, POINTARG);
			}
			return 0;

		case WM_RBUTTONDOWN:
			{
				clickMouse(false, true, POINTARG);
			}
			return 0;

		case WM_RBUTTONUP:
			{
				clickMouse(false, false, POINTARG);
			}
			return 0;

		case WM_MOUSEMOVE:
			{
				processMouse(POINTARG);
			}
			return 0;

		case WM_MOUSEWHEEL:
			{
			}
			return 0;

		case WM_DESTROY:
			{
				uninitOpenGL();
				DestroyIcon(iconSmall);
				DestroyIcon(iconBig);
				PostQuitMessage(0);
			}
			return 0;
		}

		return DefWindowProcW(window, message, wParam, lParam);
	}

	bool Main::initOpenGL()
	{
		PIXELFORMATDESCRIPTOR pfd = {0, };
		memset(&pfd, 0, sizeof(pfd));
		pfd.nSize = sizeof(pfd);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 24;
		pfd.cAlphaBits = 0;
		pfd.cAccumBits = 0;
		pfd.cDepthBits = 32;
		pfd.cStencilBits = 0;
		pfd.cAuxBuffers = 0;
		pfd.iLayerType = PFD_MAIN_PLANE;

		gldc = GetDC(window);
		int pixelFormat = ChoosePixelFormat(gldc, &pfd);
		if(pixelFormat == 0)
			return false;
		if(SetPixelFormat(gldc, pixelFormat, &pfd) == FALSE)
			return false;

		glrc = wglCreateContext(gldc);
		if(glrc == nullptr)
			return false;
		if(wglMakeCurrent(gldc, glrc) == FALSE)
			return false;

		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClearDepth(1.0);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glDepthMask(GL_TRUE);

		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

		//glEnable(GL_POLYGON_OFFSET_FILL);
		//glPolygonOffset(1.0f, 1.0f);

		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		return true;
	}

	bool Main::uninitOpenGL()
	{
		if(wglMakeCurrent(nullptr, nullptr) == FALSE)
			return false;

		if(wglDeleteContext(glrc) == FALSE)
			return false;

		glrc = nullptr;

		if(ReleaseDC(window, gldc) == FALSE)
			return false;

		gldc = nullptr;

		return true;
	}

	void Main::resize(int32_t x, int32_t y, int32_t w, int32_t h)
	{
		currentViewportX = x;
		currentViewportY = y;
		currentViewportWidth = w;
		currentViewportHeight = h;

		glViewport(currentViewportX, currentViewportY, currentViewportWidth, currentViewportHeight);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		static const double ratio = 0.0001, zNear = 0.1, zFar = 100.0;
		double frustumWidth = currentViewportWidth * ratio;
		double frustumHeight = h * ratio;
		double frustumZNear = zNear;
		double frustumZFar = zFar;
		glFrustum(-frustumWidth, frustumWidth, -frustumHeight, frustumHeight, frustumZNear, frustumZFar);
	}

	void Main::reoption()
	{
		glEnable(GL_LIGHT0);
		float position[] = {2.0f, 2.0f, 2.0f, 0.0f};
		glLightfv(GL_LIGHT0, GL_POSITION, position);
		float direction[] = {-1.0f, -1.0f, -1.0f, 0.0f};
		glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, direction);
		glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 180.0f);
		glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, 0.0f);
		float intensity[] = {1.0f, 1.0f, 1.0f, 1.0f};
		glLightfv(GL_LIGHT0, GL_AMBIENT, intensity);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, intensity);
		glLightfv(GL_LIGHT0, GL_SPECULAR, intensity);

		/*glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient.data());
		glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse.data());
		glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular.data());
		glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);*/
	}

	void Main::draw()
	{
		uint_fast32_t currentTime, timeDelta;

		currentTime = GetTickCount();
		timeDelta = currentTime - prevTime;
		prevTime = currentTime;

		if(anim)
		{
			simul.nextFrame();
#ifdef SIMULATOR_TIMER
			wchar_t frameText[100];
			int32_t total = simul.time1 + simul.time2 + simul.time3;
			swprintf_s(frameText, L"%d (%.2f) - %d (%.2f) - %d (%.2f)", simul.time1, simul.time1 * 100.0 / total, simul.time2, simul.time2 * 100.0 / total, simul.time3, simul.time3 * 100.0 / total);
			SetWindowTextW(window, frameText);
#endif
		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();

		glLoadIdentity();

		gluLookAt(cameraEyeX, cameraEyeY, cameraEyeZ, cameraAtX, cameraAtY, cameraAtZ, cameraUpX, cameraUpY, cameraUpZ);
		// gluLookAt(15.0f, 15.0f, 15.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, -1.0f);

		reoption();

		glDisable(GL_LIGHTING);

		glColor3f(1.0f, 0.0f, 0.0f); // X-axis 0-100
		glBegin(GL_LINES);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(100.0f, 0.0f, 0.0f);
		glEnd();

		glColor3f(0.0f, 1.0f, 0.0f); // Y-axis 0-100
		glBegin(GL_LINES);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 100.0f, 0.0f);
		glEnd();

		glColor3f(0.0f, 0.0f, 1.0f); // Z-axis 0-100
		glBegin(GL_LINES);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 100.0f);
		glEnd();

		glColor3f(1.0f, 1.0f, 0.0f);
		glBegin(GL_LINE_LOOP);
		glVertex3f(-1.5f, 0.0f, -0.5f);
		glVertex3f(1.5f, 0.0f, -0.5f);
		glVertex3f(1.5f, 1.0f, -0.5f);
		glVertex3f(-1.5f, 1.0f, -0.5f);
		glEnd();

		glColor3f(0.0f, 1.0f, 1.0f);
		glBegin(GL_LINE_LOOP);
		glVertex3f(-1.5f, 0.0f, -0.5f);
		glVertex3f(-1.5f, 1.0f, -0.5f);
		glVertex3f(-1.5f, 1.0f, 0.5f);
		glVertex3f(-1.5f, 0.0f, 0.5f);
		glEnd();

		glEnable(GL_LIGHTING);

		glEnable(GL_CULL_FACE);
		glPolygonMode(GL_FRONT, GL_FILL);

		glColorMaterial(GL_FRONT, GL_DIFFUSE);
		glEnable(GL_COLOR_MATERIAL);

		// draw objects
		const std::vector<Particle, AlignedAllocator<Particle>> &ptcs = simul.getParticles();

		/*glColor3f(0.3f, 0.3f, 0.3f);
		Float3 vel;
		for(auto it = ptcs.begin(); it != ptcs.end(); ++ it)
		{
			glBegin(GL_LINES);
			glVertex3f(it->pos.x, it->pos.y, it->pos.z);
			vel = it->vel; // .normalize() / 6;
			glVertex3f(it->pos.x + vel.x, it->pos.y + vel.y, it->pos.z + vel.z);
			glEnd();
		}*/

		double radius = simul.getParticleRadius();
		glColor3f(0.0f, 0.0f, 1.0f);

		//glBegin(GL_POINTS);
		for(auto it = ptcs.begin(); it != ptcs.end(); ++ it)
		{
			glPushMatrix();
			glTranslatef(it->pos.x, it->pos.y, it->pos.z);

			glutSolidSphere(radius, 5, 5, sint1, cost1, sint2, cost2);
			glPopMatrix();
			//glVertex3f(it->pos.x, it->pos.y, it->pos.z);
		}
		//glEnd();

		glPopMatrix();

		uint_fast32_t tick = GetTickCount() / 1000;
		if(prevTick != tick)
		{
#ifndef SIMULATOR_TIMER
			wchar_t frameText[100];
			swprintf_s(frameText, L"%d particles; FPS %.1f", ptcs.size(), std::accumulate(frames.begin(), frames.end(), 0.0) / 5.0);
			SetWindowTextW(window, frameText);
#endif

			prevTick = tick;
			frames[tick % 5] = 0;
		}

		++ frames[tick % 5];

#ifdef CAPTURE_FRAMES
		if(anim)
		{
			++ frameNumber;
			saveFrame();
		}
#endif
	}

	void Main::createPath(std::wstring &path)
	{
		std::vector<std::wstring> pathv = Utilities::splitAnyOf(path, L"/\\");
		std::wstring makeDir;
		ulong32_t attrib;

		path.clear();
		for(auto it = pathv.begin(); it != pathv.end(); ++ it)
		{
			if(it->empty())
				continue;

			for(int i = 0; ; ++ i)
			{
				makeDir = path + *it;
				if(i > 0)
				{
					wchar_t buf[12];
					makeDir += L" (";
					_itow_s(i, buf, 10);
					makeDir += buf;
					makeDir += L")";
				}
				attrib = GetFileAttributesW(makeDir.c_str());

				if(attrib == INVALID_FILE_ATTRIBUTES)
				{
					CreateDirectoryW(makeDir.c_str(), nullptr);
					break;
				}
				else if((attrib & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
					break;
			}

			path = makeDir + L"/";
		}
	}

	void Main::saveFrame()
	{
		size_t pixelCount = currentViewportWidth * currentViewportHeight;
		std::vector<uint8_t> data(pixelCount * 3);
		uint8_t **writeBuffer;

		glReadPixels(currentViewportX, currentViewportY, currentViewportWidth, currentViewportHeight, GL_RGB, GL_UNSIGNED_BYTE, data.data());

		FILE *fp;
		std::vector<wchar_t> fileName(capturePath.size() + 16);
		swprintf_s(fileName.data(), capturePath.size() + 16, L"%s%d.png", capturePath.c_str(), frameNumber);
		if(_wfopen_s(&fp, fileName.data(), L"wb") != 0)
			return;

		writeBuffer = new uint8_t *[currentViewportHeight];
		for(int32_t i = 0; i < currentViewportHeight; ++ i)
		{
			writeBuffer[i] = new uint8_t[currentViewportWidth * 3];
			memcpy(writeBuffer[i], data.data() + (currentViewportHeight - i - 1) * (currentViewportWidth * 3), currentViewportWidth * 3);
		}

		png_structp pptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
		png_infop iptr = png_create_info_struct(pptr);

		if(setjmp(png_jmpbuf(pptr)))
		{
			for(int32_t i = 0; i < currentViewportHeight; ++ i)
				delete [] writeBuffer[i];
			delete [] writeBuffer;

			png_destroy_write_struct(&pptr, &iptr);
			fclose(fp);
			return;
		}

		png_init_io(pptr, fp);
		png_set_IHDR(pptr, iptr, currentViewportWidth, currentViewportHeight, 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
		png_set_rows(pptr, iptr, writeBuffer);
		png_write_png(pptr, iptr, PNG_TRANSFORM_IDENTITY, nullptr);
		png_write_end(pptr, iptr);
		png_destroy_write_struct(&pptr, &iptr);
		fclose(fp);

		for(int32_t i = 0; i < currentViewportHeight; ++ i)
			delete [] writeBuffer[i];
		delete [] writeBuffer;

		swprintf_s(fileName.data(), capturePath.size() + 16, L"%s%d.txt", capturePath.c_str(), frameNumber);
		if(_wfopen_s(&fp, fileName.data(), L"wt") != 0)
			return;

		const std::vector<Particle, AlignedAllocator<Particle>> &ptcs = simul.getParticles();
		fprintf(fp, "%d\n", ptcs.size());

		for(auto it = ptcs.begin(); it != ptcs.end(); ++ it)
		{
			fprintf(fp, "%f %f %f\n", it->pos.x, it->pos.y, it->pos.z);
		}

		fclose(fp);
	}

	double Main::getCameraXZRotation()
	{
		return atan2(cameraEyeX - cameraAtX, cameraEyeZ - cameraAtZ) * 180.0 / Utilities::Constants::PI;
	}

	double Main::getCameraXZDistance()
	{
		return sqrt(pow(cameraEyeX - cameraAtX, 2) + pow(cameraEyeZ - cameraAtZ, 2));
	}

	void Main::setCameraStatus(double rotation, double xzDist)
	{
		if(xzDist < 0.2)
			xzDist = 0.2;
		else if(xzDist > 50.0)
			xzDist = 50.0;

		double curXZDist = sqrt(pow(cameraEyeX - cameraAtX, 2) + pow(cameraEyeZ - cameraAtZ, 2));

		cameraEyeX = cameraAtX + sin(rotation * Utilities::Constants::PI / 180.0) * xzDist;
		cameraEyeZ = cameraAtZ + cos(rotation * Utilities::Constants::PI / 180.0) * xzDist;
		cameraEyeY = cameraAtY + xzDist * (cameraEyeY - cameraAtY) / curXZDist;
	}

	void Main::setCameraXZRotation(double rotation)
	{
		setCameraStatus(rotation, getCameraXZDistance());
	}

	void Main::setCameraXZDistance(double xzDist)
	{
		setCameraStatus(getCameraXZRotation(), xzDist);
	}

	void Main::clickMouse(bool left, bool clicked, int32_t x, int32_t y)
	{
		if(clicked)
		{
			if(left)
				leftMouse = true;
			else
				rightMouse = true;
			trackMouse();
		}
		else
		{
			if(left)
			{
				leftMouse = false;

				/*static const float ptcDist = 0.028f;
				static const float r = 0.1f, yr = 0.15f;
				Particle p = { Float3(0, 0, 0), Float3(0, 0, 0), Float3(0, 0, 0), Float3(0, 0, 0) };
				for(float x = -r; x <= r; x += ptcDist)
				{
					for(float y = 0.7f; y <= 0.7f + yr; y += ptcDist)
					{
						for(float z = -r; z <= r; z += ptcDist)
						{
							p.pos = Float3(x, y, z);
							simul.addParticle(p);
						}
					}
				}*/
			}
			else
				rightMouse = false;
			untrackMouse();
		}

		clickOriginX = x;
		clickOriginY = y;

		clickCameraEyeX = cameraEyeX;
		clickCameraEyeY = cameraEyeY;
		clickCameraAtX = cameraAtX;
		clickCameraAtY = cameraAtY;
		clickCameraXZRotation = getCameraXZRotation();
		clickCameraDistance = getCameraXZDistance();
	}

	void Main::trackMouse()
	{
		if((leftMouse || rightMouse) && !trackingMouse)
		{
			SetCapture(window);
			trackingMouse = true;
		}
	}

	void Main::untrackMouse()
	{
		if(!leftMouse && !rightMouse && trackingMouse)
		{
			ReleaseCapture();
			trackingMouse = false;
		}
	}

	void Main::processMouse(int32_t x, int32_t y)
	{
		if(leftMouse && rightMouse) // Scaling
		{
			static const double ratio = 0.005;
			setCameraXZDistance(clickCameraDistance * exp((clickOriginY - y) * ratio));
		}
		else if(leftMouse) // Transition
		{
			/*
			static const double ratio = 0.01;
			cameraEyeX = clickCameraEyeX + (clickOriginX - x) * ratio;
			cameraEyeY = clickCameraEyeY + (y - clickOriginY) * ratio;
			cameraAtX = clickCameraAtX + (clickOriginX - x) * ratio;
			cameraAtY = clickCameraAtY + (y - clickOriginY) * ratio;
			*/
		}
		else if(rightMouse) // Rotation
		{
			static const double ratio = 0.25;
			setCameraXZRotation(fmod(clickCameraXZRotation + (clickOriginX - x) * ratio, 360.0));
		}
	}

	void Main::resetView()
	{
		cameraEyeX = 0.0;
		cameraEyeY = 0.5;
		cameraEyeZ = 1.5;
		cameraAtX = 0.0;
		cameraAtY = 0.5;
		cameraAtZ = 0.0;
		cameraUpX = 0.0;
		cameraUpY = 1.0;
		cameraUpZ = 0.0;
	}
}

int __stdcall wWinMain(HINSTANCE instance, HINSTANCE, wchar_t *commandLine, int showCommand)
{
	return Boron::Main::instance().run(instance, commandLine, showCommand) ? 0 : 1;
}
