#pragma once

#include "Simulator.h"
#include "Utilities.h"

namespace Boron
{
	class Main : public Utilities::Singleton<Main>
	{
	private:
		static const std::wstring title;
		static const int32_t windowWidth, windowHeight;

	private:
		HINSTANCE inst;
		HWND window;
		HICON iconSmall, iconBig;
		HDC gldc;
		HGLRC glrc;
		std::array<int32_t, 5> frames;

		uint32_t frameNumber;
		std::wstring capturePath;

		bool active;
		int32_t currentViewportX, currentViewportY, currentViewportWidth, currentViewportHeight;

		double cameraEyeX, cameraEyeY, cameraEyeZ;
		double cameraAtX, cameraAtY, cameraAtZ;
		double cameraUpX, cameraUpY, cameraUpZ;

		bool leftMouse, rightMouse, trackingMouse;
		int32_t clickOriginX, clickOriginY;

		double clickCameraEyeX, clickCameraEyeY;
		double clickCameraAtX, clickCameraAtY;
		double clickCameraXZRotation, clickCameraDistance;

		uint_fast32_t prevTime, prevTick;

		Simulator simul;
		bool anim;

	private:
		Main();
		~Main() {}

	public:
		HINSTANCE getInstance() { return inst; }

	public:
		bool run(HINSTANCE, const std::wstring &, int);

	private:
		longptr_t windowProcImpl(HWND, uint32_t, uintptr_t, longptr_t);

		bool initOpenGL();
		bool uninitOpenGL();

		void resize(int32_t, int32_t, int32_t, int32_t);
		void reoption();
		void draw();

		void createPath(std::wstring &);
		void saveFrame();

		double getCameraXZRotation();
		double getCameraXZDistance();
		void setCameraStatus(double, double);
		void setCameraXZRotation(double);
		void setCameraXZDistance(double);

		void clickMouse(bool, bool, int32_t, int32_t);
		void trackMouse();
		void untrackMouse();
		void processMouse(int32_t, int32_t);

		void resetView();

	private:
		static longptr_t __stdcall windowProc(HWND window, uint32_t message, uintptr_t wParam, longptr_t lParam)
		{
			return instance().windowProcImpl(window, message, wParam, lParam);
		}

		friend class Utilities::Singleton<Main>;
	};
}
