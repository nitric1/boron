// $Id$

#pragma once

#include <cmath>

#include <intrin.h>
#include <smmintrin.h>

#include <algorithm>
#include <array>
#include <fstream>
#include <functional>
#include <map>
#include <new>
#include <numeric>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#define STRICT
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX

#define WINVER 0x0610 // Windows 7
#define _WIN32_WINDOWS 0x0610
#define _WIN32_WINNT 0x0610
#define _WIN32_IE 0x0800

#include <windows.h>

#include <GL/gl.h>
#include <GL/glu.h>

#include "Types.h"

#include "../External/libpng/png.h"
