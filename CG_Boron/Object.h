// $Id$

#pragma once

#include "Utilities.h"

namespace Boron
{
	template<typename T, size_t Align = 16, size_t Block = 8>
	class AlignedAllocator : public std::allocator<T>
	{
	public:
		template<typename Other>
		struct rebind
		{
			typedef AlignedAllocator<Other, Align, Block> other;
		};

	public:
		AlignedAllocator() : std::allocator<T>() {}
		template<typename U>
		AlignedAllocator(const AlignedAllocator<U, Align, Block> &that) : std::allocator<T>(that) {}

		template<typename U>
		AlignedAllocator& operator =(const AlignedAllocator<U, Align, Block> &rhs)
		{
			std::allocator<T>::operator =(rhs);
			return *this;
		}

		pointer allocate(size_type n, const void *hint = nullptr)
		{
			pointer p = NULL;
			size_t count = sizeof(T) * n;
			size_t count_left = count % Block;
			if(count_left != 0)
			{
				count += Block - count_left;
			}

			if(!hint)
			{
				p = reinterpret_cast<pointer>(_aligned_malloc(count, Align));
			}
			else
			{
				p = reinterpret_cast<pointer>(_aligned_realloc((void *)hint, count, Align));
			}

			return p;
		}

		void deallocate(pointer p, size_type n)
		{
			_aligned_free(p);
		}

		void construct(pointer p, const T &val)
		{
			new (p) T(val);
		}

		void destroy(pointer p)
		{
			p->~T();
		}
	};


	class __declspec(align(16)) Float3
	{
	public:
		float x, y, z;
		float padding;

	public:
		Float3() : x(0.0f), y(0.0f), z(0.0f), padding(0.0f) {}
		Float3(float ix, float iy, float iz) : x(ix), y(iy), z(iz), padding(0.0f) {}
		Float3(const Float3 &that) : x(that.x), y(that.y), z(that.z), padding(0.0f) {}
		// Float3(const Float4 &that) : x(abs(that.w) < 0.000001f ? that.x : (that.x / that.w)), y(abs(that.w) < 0.000001f ? that.y : (that.y / that.w)), z(abs(that.w) < 0.000001f ? that.z : (that.z / that.w)) {}

	public:
		const Float3 operator -()
		{
			return Float3(-x, -y, -z);
		}

		Float3 &operator +=(const Float3 &rhs)
		{
			//x += rhs.x;
			//y += rhs.y;
			//z += rhs.z;
			__m128 mmLhs = _mm_load_ps(reinterpret_cast<float *>(this));
			__m128 mmRhs = _mm_load_ps(reinterpret_cast<const float *>(&rhs));
			mmLhs = _mm_add_ps(mmLhs, mmRhs);
			_mm_store_ps(reinterpret_cast<float *>(this), mmLhs);
			return *this;
		}

		Float3 &operator -=(const Float3 &rhs)
		{
			//x -= rhs.x;
			//y -= rhs.y;
			//z -= rhs.z;
			__m128 mmLhs = _mm_load_ps(reinterpret_cast<float *>(this));
			__m128 mmRhs = _mm_load_ps(reinterpret_cast<const float *>(&rhs));
			mmLhs = _mm_sub_ps(mmLhs, mmRhs);
			_mm_store_ps(reinterpret_cast<float *>(this), mmLhs);
			return *this;
		}

		Float3 &operator *=(float rhs)
		{
			//x *= rhs;
			//y *= rhs;
			//z *= rhs;
			__m128 mmLhs = _mm_load_ps(reinterpret_cast<float *>(this));
			__m128 mmRhs = _mm_load_ps1(&rhs);
			mmLhs = _mm_mul_ps(mmLhs, mmRhs);
			_mm_store_ps(reinterpret_cast<float *>(this), mmLhs);
			return *this;
		}

		Float3 &operator /=(float rhs)
		{
			//x /= rhs;
			//y /= rhs;
			//z /= rhs;
			__m128 mmLhs = _mm_load_ps(reinterpret_cast<float *>(this));
			__m128 mmRhs = _mm_load_ps1(&rhs);
			mmLhs = _mm_div_ps(mmLhs, mmRhs);
			_mm_store_ps(reinterpret_cast<float *>(this), mmLhs);
			return *this;
		}

		const Float3 operator +(const Float3 &rhs) const
		{
			return Float3(*this) += rhs;
		}

		const Float3 operator -(const Float3 &rhs) const
		{
			return Float3(*this) -= rhs;
		}

		const Float3 operator *(float rhs) const
		{
			return Float3(*this) *= rhs;
		}

		const Float3 operator /(float rhs) const
		{
			return Float3(*this) /= rhs;
		}

		bool operator ==(const Float3 &rhs) const
		{
			return x == rhs.x && y == rhs.y && z == rhs.z;
		}

		bool operator !=(const Float3 &rhs) const
		{
			return x != rhs.x || y != rhs.y || z != rhs.z;
		}

	public:
		template<typename UnaryFn>
		Float3 &applyAssign(UnaryFn fn)
		{
			x = fn(x);
			y = fn(y);
			z = fn(z);
			return *this;
		}

		template<typename UnaryFn>
		const Float3 apply(UnaryFn fn) const
		{
			return Float3(*this).applyAssign(fn);
		}

		template<typename BinaryFn>
		Float3 &applyAssign(const Float3 &rhs, BinaryFn fn)
		{
			x = fn(x, rhs.x);
			y = fn(y, rhs.y);
			z = fn(z, rhs.z);
			return *this;
		}

		template<typename BinaryFn>
		const Float3 apply(const Float3 &rhs, BinaryFn fn) const
		{
			return Float3(*this).applyAssign(rhs, fn);
		}

		Float3 &minAssign(const Float3 &rhs)
		{
			__m128 mmLhs = _mm_load_ps(reinterpret_cast<float *>(this));
			__m128 mmRhs = _mm_load_ps(reinterpret_cast<const float *>(&rhs));
			mmLhs = _mm_min_ps(mmLhs, mmRhs);
			_mm_store_ps(reinterpret_cast<float *>(this), mmLhs);
			return *this;
		}

		const Float3 min(const Float3 &rhs) const
		{
			return Float3(*this).minAssign(rhs);
		}

		Float3 &maxAssign(const Float3 &rhs)
		{
			__m128 mmLhs = _mm_load_ps(reinterpret_cast<float *>(this));
			__m128 mmRhs = _mm_load_ps(reinterpret_cast<const float *>(&rhs));
			mmLhs = _mm_max_ps(mmLhs, mmRhs);
			_mm_store_ps(reinterpret_cast<float *>(this), mmLhs);
			return *this;
		}

		const Float3 max(const Float3 &rhs) const
		{
			return Float3(*this).maxAssign(rhs);
		}

		float lengthSquared() const
		{
#ifndef NO_SSE41
			__m128 mmLhs = _mm_load_ps(reinterpret_cast<const float *>(this));
			mmLhs = _mm_dp_ps(mmLhs, mmLhs, 0x71);
			return mmLhs.m128_f32[0];
#else
			return (x * x) + (y * y) + (z * z);
#endif
		}

		float length() const
		{
			return sqrt(lengthSquared());
		}

		float distanceSquared(const Float3 &rhs) const
		{
#ifndef NO_SSE41
			__m128 mmLhs = _mm_load_ps(reinterpret_cast<const float *>(this));
			__m128 mmRhs = _mm_load_ps(reinterpret_cast<const float *>(&rhs));

			mmLhs = _mm_sub_ps(mmLhs, mmRhs);
			mmLhs = _mm_dp_ps(mmLhs, mmLhs, 0x71);

			return mmLhs.m128_f32[0];
#else
			return (x - rhs.x) * (x - rhs.x) + (y - rhs.y) * (y - rhs.y) + (z - rhs.z) * (z - rhs.z);
#endif
		}

		float distance(const Float3 &rhs) const
		{
			return sqrt(distanceSquared(rhs));
		}

		float dot(const Float3 &rhs) const
		{
#ifndef NO_SSE41
			__m128 mmLhs = _mm_load_ps(reinterpret_cast<const float *>(this));
			__m128 mmRhs = _mm_load_ps(reinterpret_cast<const float *>(&rhs));
			mmLhs = _mm_dp_ps(mmLhs, mmRhs, 0x71);

			return mmLhs.m128_f32[0];
#else
			return (x * rhs.x) + (y * rhs.y) + (z * rhs.z);
#endif
		}

		Float3 &normalizeAssign()
		{
			float len = length();
			*this /= len;
			return *this;
		}

		const Float3 normalize() const
		{
			return Float3(*this).normalizeAssign();
		}

		friend const Float3 operator *(float, const Float3 &);
	};

	class Matrix33
	{
	public:
		float a00, a01, a02, a10, a11, a12, a20, a21, a22;

	public:
		Matrix33() : a00(0.0f), a01(0.0f), a02(0.0f), a10(0.0f), a11(0.0f), a12(0.0f), a20(0.0f), a21(0.0f), a22(0.0f) {}
		Matrix33(float ia00, float ia01, float ia02, float ia10, float ia11, float ia12, float ia20, float ia21, float ia22)
			: a00(ia00), a01(ia01), a02(ia02), a10(ia10), a11(ia11), a12(ia12), a20(ia20), a21(ia21), a22(ia22) {}
		Matrix33(const Float3 &a0, const Float3 &a1, const Float3 &a2)
			: a00(a0.x), a01(a0.y), a02(a0.z), a10(a1.x), a11(a1.y), a12(a1.z), a20(a2.x), a21(a2.y), a22(a2.z) {}
		Matrix33(const Matrix33 &that)
			: a00(that.a00), a01(that.a01), a02(that.a02), a10(that.a10), a11(that.a11), a12(that.a12), a20(that.a20), a21(that.a21), a22(that.a22) {}

	public:
		const Float3 operator *(const Float3 &rhs)
		{
			return Float3(a00 * rhs.x + a01 * rhs.y + a02 * rhs.z, a10 * rhs.x + a11 * rhs.y + a12 * rhs.z, a20 * rhs.x + a21 * rhs.y + a22 * rhs.z);
		}

	public:
		Matrix33 &transposeAssign()
		{
			std::swap(a01, a10);
			std::swap(a02, a20);
			std::swap(a12, a21);
			return *this;
		}

		const Matrix33 transpose() const
		{
			return Matrix33(*this).transposeAssign();
		}
	};

	namespace Collision
	{
		class Object
		{
		public:
			virtual float determine(const Float3 &p) = 0; // f
			virtual Float3 contactPoint(const Float3 &p) = 0; // cp
			virtual float penetrationDepth(const Float3 &p) = 0; // d
			virtual Float3 normal(const Float3 &p) = 0; // n
		};

		class Sphere : public Object
		{
		private:
			Float3 center;
			float radius;
			float externalCoeff;

		public:
			Sphere(bool external, const Float3 &icenter, float iradius) : center(icenter), radius(iradius), externalCoeff(external ? -1.0f : 1.0f) {}
			Sphere(const Sphere &that) : center(that.center), radius(that.radius), externalCoeff(that.externalCoeff) {}

		public:
			virtual float determine(const Float3 &p)
			{
				return externalCoeff * (center.distanceSquared(p) - radius * radius);
			}

			virtual Float3 contactPoint(const Float3 &p)
			{
				return center + radius * ((p - center) / p.distance(center));
			}

			virtual float penetrationDepth(const Float3 &p)
			{
				return abs(center.distance(p) - radius);
			}

			virtual Float3 normal(const Float3 &p)
			{
				return Utilities::sign(determine(p)) * (center - p) / center.distance(p);
			}
		};

		class Capsule : public Object
		{
		private:
			Float3 p0, p1;
			float radius;
			float externalCoeff;

		public:
			Capsule(bool external, const Float3 &ip0, const Float3 &ip1, float iradius) : p0(ip0), p1(ip1), radius(iradius), externalCoeff(external ? -1.0f : 1.0f) {}
			Capsule(const Capsule &that) : p0(that.p0), p1(that.p1), radius(that.radius), externalCoeff(that.externalCoeff) {}

		public:
			virtual Float3 shortest(const Float3 &p) // q; shortest point on line [p0, p1] to p
			{
				return p0 + (std::min(1.0f, std::max(0.0f, -(p0 - p).dot(p1 - p0) / p1.distanceSquared(p0)))) * (p1 - p0);
			}

			virtual float determine(const Float3 &p)
			{
				return externalCoeff * (shortest(p).distance(p) - radius);
			}

			virtual Float3 contactPoint(const Float3 &p)
			{
				Float3 q = shortest(p);
				return q + radius * ((p - q) / p.distance(q));
			}

			virtual float penetrationDepth(const Float3 &p)
			{
				return abs(determine(p));
			}

			virtual Float3 normal(const Float3 &p)
			{
				Float3 q = shortest(p);
				return Utilities::sign(determine(p)) * (q - p) / q.distance(p);
			}
		};

		class OBB : public Object // oriented bounding box
		{
		private:
			Float3 center;
			Float3 a0, a1, a2; // axis; must be normalized
			Float3 dist;
			Matrix33 rotate, rotateTrans;
			float externalCoeff;

		public:
			OBB(bool external, const Float3 &icenter, const Float3 &ia0, const Float3 &ia1, const Float3 &ia2)
				: center(icenter), a0(ia0.normalize()), a1(ia1.normalize()), a2(ia2.normalize()), dist(ia0.length(), ia1.length(), ia2.length()), externalCoeff(external ? -1.0f : 1.0f)
			{
				rotate = Matrix33(a0, a1, a2);
				rotateTrans = rotate.transpose();
			}

			OBB(bool external, const Float3 &icenter, const Float3 &ia0, const Float3 &ia1, const Float3 &ia2, float idist0, float idist1, float idist2)
				: center(icenter), a0(ia0), a1(ia1), a2(ia2), dist(idist0, idist1, idist2), externalCoeff(external ? -1.0f : 1.0f)
			{
				rotate = Matrix33(a0, a1, a2);
				rotateTrans = rotate.transpose();
			}

			OBB(const OBB &that)
				: center(that.center), a0(that.a0), a1(that.a1), a2(that.a2), dist(that.dist), rotate(that.rotate), rotateTrans(that.rotateTrans), externalCoeff(that.externalCoeff) {}

		public:
			virtual Float3 local(const Float3 &p) // x_local
			{
				return rotateTrans * (p - center);
			}

			virtual float determine(const Float3 &p)
			{
				Float3 f = local(p).apply([](float f) { return abs(f); }) - dist;
				return std::max(f.x, std::max(f.y, f.z)) * externalCoeff;
			}
			
			virtual Float3 contactPointLocal(const Float3 &plocal) // cp_local
			{
				return plocal.max(-dist).min(dist);
			}

			virtual Float3 contactPoint(const Float3 &p)
			{
				return center + rotate * contactPointLocal(local(p));
			}

			virtual float penetrationDepth(const Float3 &p)
			{
				return contactPoint(p).distance(p);
			}

			virtual Float3 normal(const Float3 &p)
			{
				Float3 l = local(p);
				Float3 n = rotate * ((contactPointLocal(l) - l).apply(Utilities::sign));
				return n.normalizeAssign();
			}
		};
	}
}
