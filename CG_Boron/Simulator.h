// $Id$

#pragma once

#include "Object.h"

namespace Boron
{
	struct Particle
	{
		Float3 pos; // r(t)
		Float3 velPrevHalf; // u(t-(1/2)delta t)
		Float3 vel; // u(t) = (u(t-(1/2)delta t) + u(t+(1/2)delta t))/2
		Float3 acc; // a(t)

		bool operator ==(const Particle &rhs) const
		{
			return pos.distanceSquared(rhs.pos) < 0.00001f;
		}
	};

	class ParticleHash
	{
	private:
		static const size_t p1, p2, p3;
		float gridSize;

	public:
		ParticleHash() : gridSize(10.0f) {}
		explicit ParticleHash(float igridSize) : gridSize(igridSize) {}
		ParticleHash(const ParticleHash &that) : gridSize(that.gridSize) {}

	public:
		static Float3 hat(const Float3 &p, float gridSize)
		{
			return Float3(floor(p.x / gridSize), floor(p.y / gridSize), floor(p.z / gridSize));
		}

		static size_t hash(const Float3 &p)
		{
			return static_cast<size_t>(p.x) ^ p1 + static_cast<size_t>(p.y) ^ p2 + static_cast<size_t>(p.z) * p3;
		}

		size_t operator ()(const Float3 &p) const
		{
			return (static_cast<size_t>(floor(p.x / gridSize)) * p1) ^ (static_cast<size_t>(floor(p.y / gridSize)) * p2) ^ (static_cast<size_t>(floor(p.z / gridSize)) * p3);
		}
	};

	namespace Kernel
	{
		class Base
		{
		public:
			virtual float kernel(const Float3 &r, float h) = 0;
			virtual Float3 gradient(const Float3 &r, float h) = 0; // first derivative
			virtual float laplacian(const Float3 &r, float h) = 0; // second derivative
		};

		class SixthDegreePolynomial : public Base
		{
		public:
			virtual float kernel(const Float3 &r, float h)
			{
				float lsq = r.lengthSquared();
				if(lsq > h * h)
					return 0.0f;
				return 315.0f * pow(h * h - lsq, 3) / 64 / Utilities::Constants::PIFloat / pow(h, 9);
			}

			virtual Float3 gradient(const Float3 &r, float h)
			{
				return -945.0f * pow(h * h - r.lengthSquared(), 2) / 32 / Utilities::Constants::PIFloat / pow(h, 9) * r;
			}

			virtual float laplacian(const Float3 &r, float h)
			{
				float lsq = r.lengthSquared();
				return -945.0f * (h * h - lsq) * (3 * h * h - 7 * lsq) / 32 / Utilities::Constants::PIFloat / pow(h, 9);
			}
		};

		class Spiky : public Base
		{
		public:
			virtual float kernel(const Float3 &r, float h)
			{
				float l = r.length();
				if(l > h)
					return 0.0f;
				return 15.0f * pow(h - l, 3) / Utilities::Constants::PIFloat / pow(h, 6);
			}

			virtual Float3 gradient(const Float3 &r, float h)
			{
				float l = r.length();
				return -45.0f * r * pow(h - l, 2) / Utilities::Constants::PIFloat / pow(h, 6) / l;
			}

			virtual float laplacian(const Float3 &r, float h)
			{
				float l = r.length();
				return -90.0f * (h - l) * (h - 2 * l) / Utilities::Constants::PIFloat / pow(h, 6) / l;
			}
		};

		class Viscocity : public Base
		{
		public:
			virtual float kernel(const Float3 &r, float h)
			{
				float l = r.length();
				if(l > h)
					return 0.0f;
				return 15.0f * (-pow(l, 3) / 2 / pow(h, 3) + l * l / h / h + h / 2 / l - 1) / 2 / Utilities::Constants::PIFloat / pow(h, 3);
			}

			virtual Float3 gradient(const Float3 &r, float h)
			{
				float l = r.length();
				return 15.0f * (-3 * l / 2 / pow(h, 3) + 2 / h / h - h / 2 / pow(l, 3)) / 2 / Utilities::Constants::PIFloat / pow(h, 3) * r;
			}

			virtual float laplacian(const Float3 &r, float h)
			{
				float l = r.length();
				return 45.0f * (h - l) / Utilities::Constants::PIFloat / pow(h, 6);
			}
		};
	}

	class Simulator
	{
	public:
		struct MaterialParam
		{
			float density; // kg * m^-3
			float particleMass; // kg
			float buoyancy; // none
			float viscosity; // Pa * s
			float tension; // N * m^-1
			float threshold; // none
			float stiffness; // J
			float restitution; // none
			// float kernelParticles; // none
			float supportRadius; // m
		};

		static const MaterialParam water;
		static const Float3 gravityAcc; // m * s^-2
		static const float timeStep; // s

	private:
		static const Float3 ngbhPosVary[27];

	private:
		MaterialParam mtr; // material
		std::vector<Particle, AlignedAllocator<Particle>> ptcVec; // particles
		std::unordered_map<Float3, size_t, ParticleHash, std::equal_to<Float3>, AlignedAllocator<std::pair<Float3, size_t>>> ptcHash; // particles with hash
		std::vector<std::shared_ptr<Collision::Object>> colls; // collision objects
		std::shared_ptr<Kernel::Base> kernel, pressureKernel, viscocityKernel;

	public:
		int32_t time1, time2, time3;

	public:
		explicit Simulator(const MaterialParam &imtr)
			: mtr(imtr), ptcHash(20000, ParticleHash(imtr.supportRadius)), kernel(new Kernel::SixthDegreePolynomial()), pressureKernel(new Kernel::Spiky()), viscocityKernel(new Kernel::Viscocity()) {}
		Simulator(const MaterialParam &imtr, const std::shared_ptr<Kernel::Base> &ikernel)
			: mtr(imtr), ptcHash(20000, ParticleHash(imtr.supportRadius)), kernel(ikernel), pressureKernel(new Kernel::Spiky()), viscocityKernel(new Kernel::Viscocity()) {}
		Simulator(const Simulator &that)
			: mtr(that.mtr), ptcVec(that.ptcVec), ptcHash(that.ptcHash), kernel(that.kernel), pressureKernel(new Kernel::Spiky()), viscocityKernel(new Kernel::Viscocity()) {}
		Simulator(Simulator &&that)
			: mtr(std::move(that.mtr)), ptcVec(std::move(that.ptcVec)), ptcHash(std::move(that.ptcHash)), kernel(std::move(that.kernel)), pressureKernel(new Kernel::Spiky()), viscocityKernel(new Kernel::Viscocity()) {}

	public:
		void clear()
		{
			ptcVec.clear();
			ptcHash.clear();
			colls.clear();
		}

		void addCollision(const std::shared_ptr<Collision::Object> &obj)
		{
			colls.push_back(obj);
		}

		void addParticle(const Particle &p)
		{
			// Leapfrog integration initialization
			Particle np = p;
			np.velPrevHalf = np.vel - np.acc * timeStep / 2;
			ptcVec.push_back(static_cast<const Particle &>(np));
			ptcHash.insert(std::make_pair(p.pos, ptcVec.size() - 1));
		}

		template<typename InIt>
		void addParticles(InIt first, InIt last)
		{
			std::for_each(first, last, [this](const Particle &p)
			{
				// Leapfrog integration initialization
				Particle np = p;
				np.velPrevHalf = np.vel - np.acc * timeStep / 2;
				ptcVec.push_back(static_cast<const Particle &>(np));
				ptcHash.insert(std::make_pair(np.pos, ptcVec.size() - 1));
			});
		}

		const std::vector<Particle, AlignedAllocator<Particle>> &getParticles() const
		{
			return ptcVec;
		}

		float getParticleRadius() const
		{
			return pow(3.0f / 4.0f / Utilities::Constants::PIFloat * mtr.particleMass / mtr.density, 1.0f / 3.0f);
		}

		void nextFrame();

	private:
	};
}
