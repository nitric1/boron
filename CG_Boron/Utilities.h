#pragma once

namespace Boron
{
	namespace Utilities
	{
		class Constants
		{
		public:
			static const double PI;
			static const float PIFloat;

		private:
			~Constants();
		};

		namespace Closer
		{
			struct CloseHandleCloser
			{
				void operator ()(HANDLE h) const
				{
					if(h != nullptr && h != INVALID_HANDLE_VALUE)
						CloseHandle(h);
				}
			};
		}

		template<typename T, typename Closer = Closer::CloseHandleCloser>
		class SmartHandle
		{
		private:
			T handle;
			Closer closer;

		public:
			SmartHandle() : handle(nullptr) {}
			explicit SmartHandle(T ihandle) : handle(ihandle) {}
			SmartHandle(std::nullptr_t) : handle(nullptr) {}
			SmartHandle(SmartHandle &&obj)
				: handle(obj.handle)
			{
				obj.handle = nullptr;
			}

			~SmartHandle()
			{
				closer(handle);
			}

		public:
			T get() const
			{
				return handle;
			}
		};

		template<typename T>
		class Singleton
		{
		protected:
			~Singleton() {}

		public:
			static T &instance()
			{
				static T theInstance;
				return theInstance;
			}
			static T &inst() { return instance(); }
		};

		float sign(float);
		std::vector<std::wstring> splitAnyOf(const std::wstring &, const std::wstring &);
		std::wstring join(const std::vector<std::wstring> &, const std::wstring &);
	}
}
