// $Id$

#include "Common.h"

#include "Simulator.h"

namespace Boron
{
	namespace
	{
		// const float temp = 293.15f; // K
		// const float pressure = 101325.0f; // Pa
	}

	const size_t ParticleHash::p1 = 73856093;
	const size_t ParticleHash::p2 = 19349663;
	const size_t ParticleHash::p3 = 83492791;

	/*struct MaterialParam
	{
		float density; // kg * m^-3
		float particleMass; // kg
		float buoyancy; // none
		float viscosity; // Pa * s
		float tension; // N * m^-1
		float threshold; // none
		float stiffness; // J
		float restitution; // none
		// float kernelParticles; // none
		float supportRadius; // m
	};*/
	const Simulator::MaterialParam Simulator::water =
	{
		998.29f,
		0.02f,
		0.0f,
		4.0f, // 3.5f,
		0.0728f,
		7.065f,
		1.25f, // 3.0f,
		0.2f, // 0.0f,
		// 20.0f,
		0.0457f,
	};

	// const Float3 Simulator::gravityAcc(0.0f, -9.80665f, 0.0f);
	const Float3 Simulator::gravityAcc(0.0f, -8.0f, 0.0f);
	const float Simulator::timeStep = 0.01f;

	const Float3 Simulator::ngbhPosVary[27] =
	{
		Float3(-1.0f, -1.0f, -1.0f),
		Float3(-1.0f, -1.0f, 0.0f),
		Float3(-1.0f, -1.0f, 1.0f),
		Float3(-1.0f, 0.0f, -1.0f),
		Float3(-1.0f, 0.0f, 0.0f),
		Float3(-1.0f, 0.0f, 1.0f),
		Float3(-1.0f, 1.0f, -1.0f),
		Float3(-1.0f, 1.0f, 0.0f),
		Float3(-1.0f, 1.0f, 1.0f),
		Float3(0.0f, -1.0f, -1.0f),
		Float3(0.0f, -1.0f, 0.0f),
		Float3(0.0f, -1.0f, 1.0f),
		Float3(0.0f, 0.0f, -1.0f),
		Float3(0.0f, 0.0f, 0.0f),
		Float3(0.0f, 0.0f, 1.0f),
		Float3(0.0f, 1.0f, -1.0f),
		Float3(0.0f, 1.0f, 0.0f),
		Float3(0.0f, 1.0f, 1.0f),
		Float3(1.0f, -1.0f, -1.0f),
		Float3(1.0f, -1.0f, 0.0f),
		Float3(1.0f, -1.0f, 1.0f),
		Float3(1.0f, 0.0f, -1.0f),
		Float3(1.0f, 0.0f, 0.0f),
		Float3(1.0f, 0.0f, 1.0f),
		Float3(1.0f, 1.0f, -1.0f),
		Float3(1.0f, 1.0f, 0.0f),
		Float3(1.0f, 1.0f, 1.0f),
	};

	namespace
	{
		struct ThreadCallParam
		{
			std::function<void (size_t, size_t)> fn;
			size_t start;
			size_t space;
		};

		ulong32_t __stdcall threadCall(void *param)
		{
			ThreadCallParam *p = static_cast<ThreadCallParam *>(param);
			p->fn(p->start, p->space);
			return 0;
		}
	}

	void Simulator::nextFrame()
	{
		float suppRadiusSqd = mtr.supportRadius * mtr.supportRadius;

		size_t pcount = ptcVec.size();
		std::vector<float> massDensity(pcount), pressure(pcount);
		std::vector<Float3, AlignedAllocator<Float3>> acceleration(pcount);

		static const size_t threadCount = 8;

		ThreadCallParam params[threadCount];
		HANDLE threads[threadCount];

#ifdef SIMULATOR_TIMER
#define TIMER_COUNTER(x) QueryPerformanceCounter(&(x))
#define TIMER_FREQ() QueryPerformanceFrequency(&freq)
#define TIMER_LOG(x) (x) = static_cast<int32_t>((tick2.QuadPart - tick1.QuadPart) * 1000 / freq.QuadPart);
		LARGE_INTEGER tick1, tick2, freq;
#else
#define TIMER_COUNTER(x)
#define TIMER_FREQ()
#define TIMER_LOG(x)
#endif

		auto precalc = [&](size_t start, size_t space)
		{
			std::vector<size_t> ngbhs; // neighborhoods

			for(size_t i = start; i < pcount; i += space)
			{
				float density = 0.0f, pre = 0.0f;
				Float3 position;
				ngbhs.clear();

				position = ptcVec[i].pos;

				for(size_t j = 0; j < ARRAYSIZE(ngbhPosVary); ++ j)
				{
					size_t bucket = ptcHash.bucket(ptcVec[i].pos + ngbhPosVary[j] * mtr.supportRadius);
					for(auto bit = ptcHash.begin(bucket), bed = ptcHash.end(bucket); bit != bed; ++ bit)
					{
						if(position.distanceSquared(bit->first) <= suppRadiusSqd)
							ngbhs.push_back(bit->second);
					}
				}

				for(auto nit = ngbhs.begin(); nit != ngbhs.end(); ++ nit)
					density += mtr.particleMass * kernel->kernel(position - ptcVec[*nit].pos, mtr.supportRadius);

				massDensity[i] = density;

				pre = mtr.stiffness * (density - mtr.density);
				pressure[i] = pre;
			}
		};

#ifdef SIMULATOR_SINGLETHREAD
		TIMER_COUNTER(tick1);
		precalc(0, 1);
		TIMER_COUNTER(tick2);
		TIMER_FREQ();
		TIMER_LOG(time1);
#else
		TIMER_COUNTER(tick1);

		for(size_t i = 0; i < threadCount; ++ i)
		{
			params[i].fn = precalc;
			params[i].start = i;
			params[i].space = threadCount;
			threads[i] = CreateThread(nullptr, 65536, threadCall, &params[i], 0, nullptr);
		}

		WaitForMultipleObjects(threadCount, threads, TRUE, INFINITE);

		TIMER_COUNTER(tick2);
		TIMER_FREQ();

		TIMER_LOG(time1);
#endif

		std::vector<Particle, AlignedAllocator<Particle>> newPtcVec(pcount);

		auto calcAndUpdate = [&](size_t start, size_t space)
		{
			std::vector<size_t> ngbhs; // neighborhoods

			size_t csize = colls.size();

			for(size_t i = start; i < pcount; i += space)
			{
				float pre, density;
				float surfaceForceCoeff = 0.0f;
				Float3 position, velocity;
				Float3 surfaceNormal;
				Float3 pressureForce, viscocityForce, gravityForce, surfaceForce, buoyancyForce;
				Float3 force, acceleration;

				ngbhs.clear();

				pre = pressure[i];
				density = massDensity[i];

				position = ptcVec[i].pos;
				velocity = ptcVec[i].vel;

				for(size_t j = 0; j < ARRAYSIZE(ngbhPosVary); ++ j)
				{
					size_t bucket = ptcHash.bucket(ptcVec[i].pos + ngbhPosVary[j]);
					for(auto bit = ptcHash.begin(bucket), bed = ptcHash.end(bucket); bit != bed; ++ bit)
					{
						if(position.distanceSquared(bit->first) <= suppRadiusSqd)
							ngbhs.push_back(bit->second);
					}
				}

				for(auto nit = ngbhs.begin(); nit != ngbhs.end(); ++ nit)
				{
					if(*nit == i)
						continue;

					// internal force
					pressureForce += (pre / density / density + pressure[*nit] / massDensity[*nit] / massDensity[*nit])
						* mtr.particleMass * pressureKernel->gradient(position - ptcVec[*nit].pos, mtr.supportRadius);
					viscocityForce += (ptcVec[*nit].vel - velocity) * mtr.particleMass / massDensity[*nit]
						* viscocityKernel->laplacian(position - ptcVec[*nit].pos, mtr.supportRadius);
				}

				pressureForce *= -density;
				viscocityForce *= mtr.viscosity;

				// external force
				if(mtr.buoyancy > 0.0f) // gas
					buoyancyForce = mtr.buoyancy * (density - mtr.density) * gravityAcc;
				else // liquid
				{
					gravityForce = density * gravityAcc;
					for(auto nit = ngbhs.begin(); nit != ngbhs.end(); ++ nit)
					{
						surfaceNormal += mtr.particleMass / massDensity[*nit]
							* kernel->gradient(position - ptcVec[*nit].pos, mtr.supportRadius);
					}

					if(mtr.threshold > 0.0f && surfaceNormal.length() >= mtr.threshold)
					{
						for(auto nit = ngbhs.begin(); nit != ngbhs.end(); ++ nit)
						{
							surfaceForceCoeff += mtr.particleMass / massDensity[*nit]
								* kernel->laplacian(position - ptcVec[*nit].pos, mtr.supportRadius);
						}

						surfaceForce = -mtr.tension * surfaceForceCoeff * surfaceNormal.normalizeAssign();
					}
				}

				force = pressureForce + viscocityForce + gravityForce + surfaceForce + buoyancyForce;
				acceleration = force / density;

				Float3 nextVelocity, nextVelocityPrevHalf, nextPosition;

				nextVelocityPrevHalf = ptcVec[i].velPrevHalf + timeStep * acceleration;
				nextPosition = position + timeStep * nextVelocityPrevHalf;

				for(size_t j = 0; j < csize; ++ j)
				{
					float det = colls[j]->determine(nextPosition);
					if(det > 0.0f)
					{
						Float3 normal = colls[j]->normal(nextPosition);
						float penDepth = colls[j]->penetrationDepth(nextPosition);
						Float3 position = colls[j]->contactPoint(nextPosition);
						Float3 velocity = nextVelocityPrevHalf
							- (1 + mtr.restitution * penDepth / timeStep / nextVelocityPrevHalf.length())
							* nextVelocityPrevHalf.dot(normal) * normal;
						nextPosition = position;
						nextVelocityPrevHalf = velocity;
					}
				}

				newPtcVec[i].pos = nextPosition;
				newPtcVec[i].acc = acceleration;
				nextVelocity = (ptcVec[i].velPrevHalf + nextVelocityPrevHalf) / 2.0f;
				newPtcVec[i].vel = nextVelocity;
				newPtcVec[i].velPrevHalf = nextVelocityPrevHalf;
			}
		};

#ifdef SIMULATOR_SINGLETHREAD
		TIMER_COUNTER(tick1);
		calcAndUpdate(0, 1);
		TIMER_COUNTER(tick2);
		TIMER_FREQ();
		TIMER_LOG(time2);
#else
		TIMER_COUNTER(tick1);

		for(size_t i = 0; i < threadCount; ++ i)
		{
			params[i].fn = calcAndUpdate;
			params[i].start = i;
			params[i].space = threadCount;
			threads[i] = CreateThread(nullptr, 65536, threadCall, &params[i], 0, nullptr);
		}

		WaitForMultipleObjects(threadCount, threads, TRUE, INFINITE);

		TIMER_COUNTER(tick2);
		TIMER_FREQ();

		TIMER_LOG(time2);
#endif

		TIMER_COUNTER(tick1);

		ptcHash.clear();
		ptcHash.bucket_size(20000);

		ptcVec.clear();
		for(auto it = newPtcVec.begin(); it != newPtcVec.end(); ++ it)
		{
			if(_isnan(it->pos.x) || _isnan(it->pos.y) || _isnan(it->pos.z))
				continue;
			ptcVec.push_back(*it);
		}

		pcount = ptcVec.size();
		for(size_t i = 0; i < pcount; ++ i)
			ptcHash.insert(std::make_pair(ptcVec[i].pos, i));

		TIMER_COUNTER(tick2);
		TIMER_FREQ();

		TIMER_LOG(time3);
	}
}
