#include "Common.h"

#include "Utilities.h"

namespace Boron
{
	namespace Utilities
	{
		const double Constants::PI = 3.14159265358979;
		const float Constants::PIFloat = 3.14159265358979f;

		float sign(float n)
		{
			if(n < 0.0f)
				return -1.0f;
			else if(n > 0.0f)
				return 1.0f;
			return 0.0f;
		}

		std::vector<std::wstring> splitAnyOf(const std::wstring &str, const std::wstring &sep)
		{
			size_t ppos, pos = static_cast<size_t>(-1);
			std::vector<std::wstring> ve;

			while(true)
			{
				ppos = pos;
				pos = str.find_first_of(sep, ++ ppos);

				if(pos == std::wstring::npos)
				{
					ve.push_back(str.substr(ppos));
					break;
				}
				else
					ve.push_back(str.substr(ppos, pos - ppos));
			}

			return ve;
		}

		std::wstring join(const std::vector<std::wstring> &ve, const std::wstring &sep)
		{
			if(ve.empty())
				return std::wstring();

			std::wstring str = ve.front();
			for(auto it = ++ ve.begin(); it != ve.end(); ++ it)
			{
				str += sep;
				str += *it;
			}

			return str;
		}
	}
}
