// $Id$

#include "Common.h"

#include "Object.h"

namespace Boron
{
	const Float3 operator *(float lhs, const Float3 &rhs)
	{
		return rhs * lhs;
	}
}
