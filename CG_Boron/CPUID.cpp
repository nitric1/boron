#include "Common.h"

#include "CPUID.h"

namespace Boron
{
	bool isSSE41Supported()
	{
		CPUIDInfo info;
		if(isCPUIDSupported())
		{
			getCPUID(&info, 1, 0);
			if((info.ecx & 0x00080000u) > 0)
				return true;
		}
		return false;
	}
}
