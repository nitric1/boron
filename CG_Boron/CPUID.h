#pragma once

struct CPUIDInfo
{
	uint32_t eax, ebx, ecx, edx;
};

extern "C"
{
	int isCPUIDSupported();
	void getCPUID(CPUIDInfo *info, uint32_t leaf, uint32_t subleaf);
};

namespace Boron
{
	bool isSSE41Supported();
}
